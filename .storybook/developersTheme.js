import { create } from '@storybook/theming/create';

export default create({
    barTextColor: 'silver',
    barSelectedColor: 'black',
    barBg: 'hotpink',
    brandTitle: 'Developers.nl storybook',
    brandUrl: 'https://developers.nl',
    brandImage: 'https://developers.nl/_next/static/images/logo-a0939ca8aeebe45f1e0743956ff71baa.svg',
});
