module.exports = {
    stories: ['../src/stories/*.stories.mdx'],
    addons: ['@storybook/preset-typescript', '@storybook/addon-docs'],
};
