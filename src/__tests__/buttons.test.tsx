import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { Button } from '../components/Button/Button';

it('should render correctly', () => {
    const tree = renderer.create(<Button></Button>).toJSON();
    expect(tree).toMatchSnapshot();
});
